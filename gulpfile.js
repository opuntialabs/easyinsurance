var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var pug = require('gulp-pug');

gulp.task('sass', function(){
  return gulp.src('app/scss/*.scss')
    .pipe(sass({outputStyle: 'expanded'})) // Converts Sass to CSS with gulp-sass
    .pipe(autoprefixer())
    .pipe(gulp.dest('dist/css'))
});

gulp.task('pug', function buildHTML() {
  return gulp.src('app/pug/*.pug')
  .pipe(pug({
    // Your options in here. 
        pretty: true,
        exclude: '_partials/**/*',
        src: 'app/pug/',
        dest: 'dist/',
        search: '**/*.pug',
        pugExtension: '.pug',
  }))
  .pipe(gulp.dest('dist/'))
});

gulp.task('watch', function(){
  gulp.watch('app/scss/*.scss', ['sass']);
  gulp.watch('app/pug/*.pug', ['pug']); 
  // Other watchers
})